import logo from "./logo.svg";
import "./App.css";
import Redux_Phone from "./Redux_Phone/Redux_Phone";

function App() {
  return (
    <div className="App">
      <Redux_Phone />
    </div>
  );
}

export default App;

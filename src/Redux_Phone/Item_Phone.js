import React, { Component } from "react";
import { connect } from "react-redux";

class Item_Phone extends Component {
  render() {
    let {
      maSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      hinhAnh,
      tenSP,
      giaBan,
    } = this.props.phone;
    return (
      <div className="col-4 p-4">
        <div className="card border-primary">
          <img className="card-img-top" src={hinhAnh} alt />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
            <button
              onClick={() => {
                this.props.changerPhone(this.props.phone);
              }}
              className="btn btn-success mx-3"
            >
              Xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.handlePushToCart(this.props.phone);
              }}
              className="btn btn-primary"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    changerPhone: (phone) => {
      let action = {
        type: "XEM_CHI_TIET",
        payload: phone,
      };
      dispatch(action);
    },
    handlePushToCart: (phone) => {
      let action = {
        type: "ADD_TO_CART",
        payload: phone,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(Item_Phone);

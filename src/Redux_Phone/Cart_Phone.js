import React, { Component } from "react";
import { connect } from "react-redux";

class Cart_Phone extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.maSP}</td>
          <td>{item.tenSP}</td>
          <td>
            <img style={{ width: 50 }} src={item.hinhAnh} alt="" />
          </td>
          <td>{item.giaBan * item.soluong}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangerQuatityPhone(item.maSP, -1);
              }}
              className="btn btn-success"
            >
              -
            </button>
            <strong>{item.soluong}</strong>
            <button
              onClick={() => {
                this.props.handleChangerQuatityPhone(item.maSP, +1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDeletePhone(item.maSP);
              }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h2>Cart_Phone</h2>
        <table className="table">
          <thead>
            <tr>
              <th>Mã</th>
              <th>Tên SP</th>
              <th>Img</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.phoneReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDeletePhone: (maSPphone) => {
      let action = {
        type: "DELETE",
        payload: maSPphone,
      };
      dispatch(action);
    },
    handleChangerQuatityPhone: (maSPphone, luachon) => {
      let action = {
        type: "CHANGE_QUANTITY",
        payload: { maSPphone: maSPphone, luachon },
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart_Phone);

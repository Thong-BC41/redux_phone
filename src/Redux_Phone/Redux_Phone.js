import React, { Component } from "react";
import Cart_Phone from "./Cart_Phone";
import DetaiPhone from "./DetaiPhone";
import List_Phone from "./List_Phone";

export default class Phone extends Component {
  render() {
    return (
      <div className="container">
        <Cart_Phone />
        <List_Phone />
        <DetaiPhone />
      </div>
    );
  }
}

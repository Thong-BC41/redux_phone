import React, { Component } from "react";
import { connect } from "react-redux";
import Item_Phone from "./Item_Phone";

class List_Phone extends Component {
  renderPhone = () => {
    return;
  };
  render() {
    return (
      <div className="row">
        {this.props.list.map((item) => {
          return (
            <Item_Phone
              phone={item}
              // changerPhone={this.props.changerPhone}
            />
          );
        })}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.phoneReducer.listShop,
  };
};

export default connect(mapStateToProps)(List_Phone);

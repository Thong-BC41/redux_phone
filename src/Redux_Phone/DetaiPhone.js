import React, { Component } from "react";
import { connect } from "react-redux";

class DetaiPhone extends Component {
  render() {
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.phone;
    return (
      <div className="row">
        <img className="col-4" src={hinhAnh} alt="" />
        <div className="col-8   ">
          <h2>{maSP}</h2>
          <h2>{tenSP}</h2>
          <h2>{manHinh}</h2>
          <h2>{heDieuHanh}</h2>
          <h2>{cameraTruoc}</h2>
          <h2>{cameraSau}</h2>
          <h2>{ram}</h2>
          <h2>{rom}</h2>
          <h2>{giaBan}</h2>
          <h2>{hinhAnh}</h2>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    phone: state.phoneReducer.detail,
  };
};

export default connect(mapStateToProps)(DetaiPhone);

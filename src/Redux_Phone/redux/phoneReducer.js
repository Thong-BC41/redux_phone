import { data_phone } from "../Data_Phone";

let initalValue = { listShop: data_phone, detail: data_phone[0], cart: [] };

export const phoneReducer = (state = initalValue, action) => {
  switch (action.type) {
    case "XEM_CHI_TIET": {
      state.detail = action.payload;
      return { ...state };
    }
    case "ADD_TO_CART": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.maSP == action.payload.maSP;
      });
      if (index == -1) {
        let newPhone = { ...action.payload, soluong: 1 };
        cloneCart.push(newPhone);
      } else {
        cloneCart[index].soluong++;
      }
      return { ...state, cart: cloneCart };
    }
    case "DELETE": {
      let newcart = state.cart.filter((item) => {
        return item.maSP != action.payload;
      });
      return { ...state, cart: newcart };
    }
    case "CHANGE_QUANTITY": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.maSP == action.payload.maSPphone;
      });
      cloneCart[index].soluong += action.payload.luachon;
      if (cloneCart[index].soluong >= 1) {
        return { ...state, cart: cloneCart };
      } else {
        let newcart = state.cart.filter((item) => {
          return item.maSP != action.payload.maSPphone;
        });
        return { ...state, cart: newcart };
      }
    }

    default:
      return { ...state };
  }
};

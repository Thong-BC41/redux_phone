import { combineReducers } from "redux";
import { phoneReducer } from "./phoneReducer";

export const rootReducer_Phone = combineReducers({
  phoneReducer,
});
